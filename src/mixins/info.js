import { mapState, mapActions} from 'vuex'

export const getInformation = {
	computed: {
		...mapState(['movements', 'acounts', 'vendors'])
	},
	methods:{
		...mapActions([ 'getMovements', 'getAcounts', 'getVendors' ])
	},
	async created(){
    await this.getMovements();
    await this.getAcounts();
    await this.getVendors();
  }
}
