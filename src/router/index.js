import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "Home" */ '../views/Home.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/producto',
    name: 'Producto',
    component: () => import(/* webpackChunkName: "Producto" */ '../views/Productos.vue')
  },
  {
    path: '/fondos',
    name: 'Fondo',
    component: () => import(/* webpackChunkName: "Fondos" */ '../views/Fondos.vue')
  },
  {
    path: '/movements',
    name: 'Movements',
    component: () => import(/* webpackChunkName: "Movements" */ '../views/movements/Movements.vue')
  },
  {
    path: '/movements/new',
    name: 'new',
    component: () => import(/* webpackChunkName: "movements/new" */ '../views/movements/New.vue')
  },
  {
    path: '/vendors',
    name: 'Vendors',
    component: () => import(/* webpackChunkName: "Vendors" */ '../views/vendors/Vendors.vue')
  },
  {
    path: '/vendors/new',
    name: 'new',
    component: () => import(/* webpackChunkName: "Vendors/new" */ '../views/vendors/New.vue')
  },
  {
    path: '/coins',
    name: 'coins',
    component: () => import(/* webpackChunkName: "coins" */ '../views/coins/Coins.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
