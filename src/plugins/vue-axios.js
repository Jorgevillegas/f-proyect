import Vue from 'vue'
import Axios from 'axios'
import VueAxios from 'vue-axios'

const axios = Axios.create({
  baseURL: null,
  showLoading: true
})

Vue.use(VueAxios, axios)

export default axios
