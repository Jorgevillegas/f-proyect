import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  data:{
    url: 'http://192.168.2.227:3000',
  },
  state: {
    paramsMovement: [],
    api : [],
    acounts: [],
    movements: [],
    movement: null,
    vendors: null,
    vendor: null,
    coins: null,
    coin: null,
    productos: [
      {nombre: "Carro",
       precio: 1600.00,
       cantidad: 2
      },
      {nombre: "Casa",
       precio: 25000.00,
       cantidad: 4
      }
    ]
  },
  mutations: {
    setapi(state, request){
      state.api = request
    },
    setAcounts(state, request){
      state.acounts = request
    },
    setMovements(state, request){
      state.movements = request
    },
    setVendors(state, request){
      state.vendors = request
    },
    setCoins(state, request){
      state.coins = request
    },
    setVendorCreated(state, request){
      state.vendor = request
    },
    setMovementCreated(state, request){
      state.movement = request
    }
  },
  actions: {
    geting: async function ({commit}) {
      const request = await this.$http.get('https://s3.amazonaws.com/dolartoday/data.json')
      commit('setapi',request.data)
    },
    getAcounts: async function ({commit}) {
      // const data = await this.$http.get('http://192.168.2.227:3000/fondos.json');
      const acounts = await this.$http.get('http://192.168.27.64:3000/acounts');
      commit('setAcounts',acounts.data)
    },
    getMovements: async function ({commit}) {
      const movements = await this.$http.get('http://192.168.27.64:3000/movements')
      commit('setMovements', movements.data)
    },
    getVendors: async function ({commit}) {
      const vendors = await this.$http.get('http://192.168.27.64:3000/vendors')
      commit('setVendors', vendors.data)
    },
    getCoins: async function ({commit}) {
      const coins = await this.$http.get('http://192.168.27.64:3000/coins')
      commit('setCoins', coins.data)
    },
    createMovement: async function (_, params) {
      console.log(params)
      await this.$http.post('http://192.168.27.64:3000/movements', params)
    },
    createVendor: async function (_, params) {
      await this.$http.post('http://192.168.27.64:3000/vendors', params)
    },
    createCoin: async function (_, params) {
      await this.$http.post('http://192.168.27.64:3000/coins', params)
    }
  }
})
store.$http = store._vm.$http

export default store