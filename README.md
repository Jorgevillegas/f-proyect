# f-proyect

## Project setup
```
npm install
```
```
npm install yarn
```

```
npm install axios
```

```
npm install bootstrap-vue
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
